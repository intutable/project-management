// Tables
export const ROLES = "roles"
export const PROJECTS = "projects"
export const TABLES = "tables"
export const COLUMNS = "columns"
export const ROLES_PROJECTS = "roles_projects"
export const PROJECTS_TABLES = "projects_tables"

// Columns
export const ID = "_id"
export const NAME = "name"
export const PROJECT_ID = "project_id"
export const PROJECT_NAME = "project_name"
export const OWNER_ID = "owner_id"
export const KEY = "key"
export const TABLE_ID = "table_id"
export const COLUMN_NAME = "column_name"
export const TYPE = "type"
export const PASSWORD = "password"
export const ROLE_ID = "role_id"
export const TABLE_NAME = "name"
export const DEFAULT = "default"

export const COLUMN_BASE_ATTRIBUTES = [ID, COLUMN_NAME, TYPE, TABLE_ID]
